/* eslint-disable no-unused-expressions */
const { expect } = require('chai');
const url = require('../models/url');

describe('url', () => {
  context('with valid url', () => {
    const url1 = 'http://google.com/image1.png';
    const url2 = 'sftp://hello.com/largeFile/12/100gb.zip';

    it('should return protocol', () => {
      expect(url.getInfo(url1).protocol).to.equal('http');
      expect(url.getInfo(url2).protocol).to.equal('sftp');
    });

    it('should return host', () => {
      expect(url.getInfo(url1).host).to.equal('google.com');
      expect(url.getInfo(url2).host).to.equal('hello.com');
    });

    it('should return filename', () => {
      expect(url.getInfo(url1).filename).to.equal('image1.png');
      expect(url.getInfo(url2).filename).to.equal('100gb.zip');
    });

    it('should return pathname', () => {
      expect(url.getInfo(url1).pathname).to.equal('/image1.png');
      expect(url.getInfo(url2).pathname).to.equal('/largeFile/12/100gb.zip');
    });
  });

  context('with url missing protocol', () => {
    const invalidUrl = 'google.com';
    it('should return error', () => {
      expect(url.getInfo(invalidUrl)).to.be.an('object');
      expect(url.getInfo(invalidUrl)).to.have.property('error').that.is.a('string');
    });
  });

  context('with url missing filename', () => {
    const invalidUrl = 'http://hello.com';
    it('should return error', () => {
      expect(url.getInfo(invalidUrl)).to.be.an('object');
      expect(url.getInfo(invalidUrl)).to.have.property('error').that.is.a('string');
    });
  });

  context('with invalid url', () => {
    const invalidUrl = 'http://abc';
    it('should return error', () => {
      expect(url.getInfo(invalidUrl)).to.be.an('object');
      expect(url.getInfo(invalidUrl)).to.have.property('error').that.is.a('string');
    });
  });
});
