/* eslint-disable no-unused-expressions */
const { expect } = require('chai');
const fs = require('fs');
const path = require('path');
const { protocols } = require('../models/protocols');
const urlParser = require('../models/url');

describe('protocol', () => {
  Object.keys(protocols).forEach((key) => {
    const protocol = protocols[key];

    context(`${key} required properties`, () => {
      context('should have property info', () => {
        it('is not undefined', () => {
          expect(protocol.info).to.not.be.undefined;
        });

        it('have string name', () => {
          expect(protocol.info.name).to.be.a('string').to.not.be.empty;
        });
      });

      context('should have property requirements', () => {
        it('is not undefined', () => {
          expect(protocol.requirements).to.not.be.undefined;
        });

        it('is an array', () => {
          expect(protocol.requirements).to.be.an('array');
        });
      });

      context('should have function download', () => {
        it('is a function', () => {
          expect(protocol.download).to.be.a('function');
        });
      });
    });

    context(`${key} download function`, () => {
      context('on error', () => {
        const invalidUrl = `${protocol.info.name}://goog/co`;
        const localPath = __dirname;

        it('should reject with error message object', async () => {
          const urlInfo = urlParser.getInfo(invalidUrl);
          try {
            await protocol.download(urlInfo, localPath, {});
          } catch (error) {
            expect(error.message).to.be.a('string');
          }
        });

        it('should not have file leftover', () => {
          const urlInfo = urlParser.getInfo(invalidUrl);
          const filePath = path.join(localPath, urlInfo.filename);
          expect(fs.existsSync(filePath)).to.be.false;
        });
      });
    });
  });
});
