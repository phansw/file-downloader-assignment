/* eslint-disable no-console */
const inquirer = require('inquirer');
const chalk = require('chalk');

exports.queryDownloadLocation = async function viewQueryDownloadLocation() {
  return inquirer.prompt([
    {
      type: 'input',
      name: 'downloadLocation',
      message: 'Download path',
      default: './',
    },
  ]);
};

exports.queryRequirements = async function viewQueryRequirements(url, requirements) {
  console.log();
  console.log(chalk.blue(url));
  return inquirer.prompt(requirements);
};

exports.downloadStart = function viewDownloadStart(url) {
  console.log();
  console.log(chalk.yellow(`Download started for ${url}`));
};

exports.downloadComplete = function viewDownloadComplete(url) {
  console.log();
  console.log(chalk.green(`Download completed for ${url}`));
};

exports.error = function viewError(e) {
  console.log();
  console.log(chalk.red(e));
};
