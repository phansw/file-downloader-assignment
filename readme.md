# File Downloader Assignment

File downloader to download list of files from urls

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for 
development and testing purposes.

### Prerequisites

You should have a recent version of node (>= v10) installed.

### Installing

Just go to the root directory, install node packages, and run node on index.js, with a list of urls you want to download.
Sample below:

```
npm install
node index ftp://speedtest.tele2.net/1MB.zip https://techcrunch.com/wp-content/uploads/2015/04/codecode.jpg sftp://test.rebex.net/readme.txt
```

FTP username: anonymous, password: anything

SFTP username: demo, password: password

### Testing
```
npm test
```

### Supported protocols
HTTP, HTTPS, FTP, SFTP

## To Do:
* Integration Tests - setup test environment servers to test downloads
