const fs = require('fs');
const path = require('path');

exports.createDirectoryIfNotExists = function createDirectoryIfNotExists(directoryPath) {
  const directories = path.normalize(directoryPath).split(path.sep);
  let currentPath = '';

  directories.forEach((directory) => {
    currentPath = path.join(currentPath, directory);
    if (!fs.existsSync(currentPath)) {
      fs.mkdirSync(currentPath);
    }
  });
};
