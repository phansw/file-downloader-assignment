const view = require('./view');
const urlParser = require('./models/url');
const { protocols } = require('./models/protocols');
const utility = require('./utility');

exports.download = async function controllerDownload(urls, saveLocation) {
  let localPath = saveLocation;

  if (saveLocation === undefined || saveLocation.length === 0) {
    const { downloadLocation } = await view.queryDownloadLocation();
    localPath = downloadLocation;
    utility.createDirectoryIfNotExists(localPath);
  }

  const urlInfos = urls
    .map(url => ({ url, ...urlParser.getInfo(url) }));

  const downloadTasks = [];

  for (let i = 0; i < urlInfos.length; i += 1) {
    if (urlInfos[i].error) {
      view.error(urlInfos[i].error);
      continue;
    }

    const { protocol: urlProtocol, filename } = urlInfos[i];
    if (!filename || filename.length === 0) continue;
    const protocol = protocols[urlProtocol];
    if (protocol) {
      const { url } = urlInfos[i];
      let extraInfo = {};
      if (protocol.requirements.length > 0) {
        // eslint-disable-next-line no-await-in-loop
        extraInfo = await view.queryRequirements(url, protocol.requirements);
      }

      downloadTasks.push(() => {
        protocol
          .download(urlInfos[i], localPath, extraInfo)
          .then(() => {
            view.downloadComplete(url);
          })
          .catch((error) => {
            view.error(error.message);
          });

        view.downloadStart(url);
      });
    }
  }

  downloadTasks.forEach(task => task());
};
