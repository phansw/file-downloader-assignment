const path = require('path');
const fs = require('fs');
const request = require('request');
const protocolUtility = require('./utility');

exports.info = {
  name: 'http',
};

exports.requirements = [];

exports.download = async function httpDownload(urlInfo, localPath) {
  const { url, filename } = urlInfo;
  const filePath = path.join(localPath, filename);
  return new Promise((resolve, reject) => {
    request(url)
      .on('response', () => {
        resolve();
      })
      .on('error', (e) => {
        protocolUtility.cleanUp(filePath);
        reject(protocolUtility.error(url, e));
      })
      .pipe(fs.createWriteStream(filePath));
  });
};
