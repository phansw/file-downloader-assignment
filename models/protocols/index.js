const normalizedPath = require('path').join(__dirname);
const exclude = ['index.js', 'utility.js'];

exports.protocols = {};

require('fs').readdirSync(normalizedPath).forEach((file) => {
  if (exclude.indexOf(file) === -1) {
    // eslint-disable-next-line import/no-dynamic-require,global-require
    const protocol = require(`./${file}`);
    exports.protocols[protocol.info.name] = protocol;
  }
});
