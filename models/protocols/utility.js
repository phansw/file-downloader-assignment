const fs = require('fs');

exports.cleanUp = function protocolUtilityCleanup(filePath) {
  fs.unlink(filePath, () => {
  });
};

exports.error = function protocolUtilityError(url, error) {
  return { message: `Error downloading ${url} - ${error.code}` };
};
