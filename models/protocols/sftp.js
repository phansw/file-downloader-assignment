const path = require('path');
const fs = require('fs');
const Client = require('ssh2-sftp-client');
const protocolUtility = require('./utility');

exports.info = {
  name: 'sftp',
};

exports.requirements = [
  {
    type: 'input',
    name: 'username',
    message: 'Username',
  },
  {
    type: 'password',
    name: 'password',
    message: 'Password',
  },
];

exports.download = async function sftpDownload(urlInfo, localPath, extraInfo) {
  const { host, filename, pathname } = urlInfo;
  const { username, password } = extraInfo;
  const sftp = new Client();

  const filePath = path.join(localPath, filename);
  const fsStream = fs.createWriteStream(filePath);

  try {
    await sftp.connect({
      host,
      port: 22, // assume running on port 22
      username,
      password,
    });
  } catch (error) {
    sftp.end();
    protocolUtility.cleanUp(filePath);
    return Promise.reject(protocolUtility.error(urlInfo.url, error.toString()));
  }

  return sftp.get(pathname, fsStream)
    .then(() => sftp.end())
    .catch((error) => {
      sftp.end();
      protocolUtility.cleanUp(filePath);
      return protocolUtility.error(urlInfo.url, error.toString());
    });
};
