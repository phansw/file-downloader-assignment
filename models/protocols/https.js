const http = require('./http');

exports.info = {
  name: 'https',
};

exports.requirements = http.requirements;

exports.download = http.download;
