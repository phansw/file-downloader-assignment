const path = require('path');
const fs = require('fs');
const Client = require('ftp');
const protocolUtility = require('./utility');

exports.info = {
  name: 'ftp',
};

exports.requirements = [
  {
    type: 'input',
    name: 'username',
    message: 'Username',
  },
  {
    type: 'password',
    name: 'password',
    message: 'Password',
  },
];

exports.download = async function sftpDownload(urlInfo, localPath, extraInfo) {
  const { host, filename, pathname } = urlInfo;
  const { username, password } = extraInfo;
  const ftp = new Client();
  const filePath = path.join(localPath, filename);

  return new Promise((resolve, reject) => {
    ftp.on('ready', () => {
      ftp.get(pathname, (err, stream) => {
        if (err) reject(err);
        const fsStream = fs.createWriteStream(filePath);
        stream.once('close', () => {
          ftp.end();
          resolve();
        });
        stream.pipe(fsStream);
      });
    });

    ftp.on('error', (err) => {
      protocolUtility.cleanUp(filePath);
      reject(protocolUtility.error(urlInfo.url, err.code));
    });

    ftp.connect({
      host,
      port: 21, // assume running on port 21
      username,
      password,
    });
  });
};
