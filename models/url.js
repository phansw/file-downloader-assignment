const path = require('path');

exports.getInfo = function urlGetInfo(url) {
  let urlObject = {};

  try {
    urlObject = new URL(url);
  } catch (e) {
    return { error: `Invalid url: ${url}` };
  }

  const filename = path.basename(urlObject.pathname);
  if (!filename || filename.length === 0) {
    return { error: `Invalid url: ${url} - missing filename` };
  }

  const protocol = urlObject.protocol.replace(':', '');
  const { host, pathname } = urlObject;

  return {
    protocol,
    host,
    filename,
    pathname,
  };
};
