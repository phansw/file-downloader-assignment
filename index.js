const program = require('commander');
const controller = require('./controller');

program.version('0.1.0')
  .option('-o --output [output]', 'Download location')
  .parse(process.argv);

controller.download(program.args, program.output);
